#include <stdio.h>
void reverseSentence();
int main() {
    printf("Enter a sentence: ");
    reverseSentence();
    return 0;
}

void reverseSentence() {
    char rev;
    scanf("%c", &rev);
    if (rev != '\n') {
        reverseSentence();
        printf("%c",rev);
    }
}
